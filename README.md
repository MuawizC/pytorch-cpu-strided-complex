# pytorch-cpu-strided-complex

PyTorch extension that adds support for:

* device: cpu
* layout: strided
* dtype: complex64, complex128

## Install

```
git clone git@gitlab.com:pytorch-complex/pytorch-cpu-strided-complex.git
cd pytorch-cpu-strided-complex
rm -rf build/ && rm -rf dist/  # Always remove build and dist cached folders
python setup.py install
```

## Test

```
cd test
python test_torch.py
python test_autograd.py
```

## Missing Ops

Due to the conversion from TH to ATen, the following kernels are missing:

```
_th_take()
_th_index_select()
_th_masked_select_bool()
_th_gather()
__th_random_()
_th_uniform()
_th_normal()
_th_min()
_th_max()
_th_set_()
_th_fmod_()
_th_geqrf()
_th_orgqr()
_th_ormqr()
_th_gels()
_th_ger()
_th_dot()
_th_eig()
_th_mv()
_th_mm()

```