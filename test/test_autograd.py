import unittest

import torch as th
import numpy as np
import scipy as sp
from scipy import linalg as sp_linalg

from torch_cpu_strided_complex import cpp
from numpy.testing import *


def t2n(t):
    return t.detach().numpy()

device = th.device('cpu')
# device = th.device('cuda:0')


class TestComplexAutograd(unittest.TestCase):

    def test_abs(self):
        def _test_real(sizes):
            x = torch.randn(sizes, requires_grad=True, dtype=torch.double)

            def abs(x):
                return x.abs()

            gradcheck(abs, [x])
            gradgradcheck(abs, [x])

    def test_angle(self):
        def _test_real(sizes):
            x = torch.randn(sizes, requires_grad=True, dtype=torch.double)

            def angle(x):
                return x.angle()

            gradcheck(angle, [x])
            gradgradcheck(angle, [x])

    def test_real(self):
        def _test_real(sizes):
            x = torch.randn(sizes, requires_grad=True, dtype=torch.double)

            def real(x):
                return x.real()

            gradcheck(real, [x])
            gradgradcheck(real, [x])

    def test_imag(self):
        def _test_real(sizes):
            x = torch.randn(sizes, requires_grad=True, dtype=torch.double)

            def imag(x):
                return x.imag()

            gradcheck(imag, [x])
            gradgradcheck(imag, [x])

    def test_conj(self):
        def _test_real(sizes):
            x = torch.randn(sizes, requires_grad=True, dtype=torch.double)

            def conj(x):
                return x.conj()

            gradcheck(conj, [x])
            gradgradcheck(conj, [x])


if __name__ == '__main__':
    unittest.main()
