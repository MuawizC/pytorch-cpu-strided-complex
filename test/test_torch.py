import unittest

import torch as th
import numpy as np
import scipy as sp
from scipy import linalg as sp_linalg

from torch_cpu_strided_complex import cpp
# from torch_cuda_strided_complex import cuda
from numpy.testing import *

dtype_map = {th.float32: np.float32,
             th.float64: np.float64,
             th.complex64: np.complex64,
             th.complex128: np.complex128}

c2r_dtype = {th.complex64: th.float32,
             th.complex128: th.float64}

devices = (th.device('cpu'),)
# devices = (th.device('cpu'), th.device('cuda:0'))


def t2n(t):
    return t.detach().cpu().numpy()


class TestComplexTensor(unittest.TestCase):

    def test_empty(self):
        for device in devices:
            for dtype in (th.complex64, th.complex128):
                th_kwargs = {"dtype": dtype, "device": device}
                th.empty((2, 2), **th_kwargs)

    @unittest.expectedFailure
    def test_print(self):
        for device in devices:
            for dtype in (th.complex64, th.complex128):
                th_kwargs = {"dtype": dtype, "device": device}
                a = th.empty((2, 2), **th_kwargs)

                a.__repr__()  # _th_masked_select_bool

    def test_to(self):
        for device in devices:
            for dtype in (th.complex64, th.complex128):
                th_kwargs = {"dtype": dtype, "device": device}
                tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).tiny}
                a = th.tensor([[1+1j, 1-1j], [2.51-2.51j, 2.51+2.51j]], **th_kwargs)

                a.to(th.float64)
                assert_allclose(t2n(a.type(th.uint8)), t2n(a).astype(np.uint8), **tol_kwargs)

    def test_copy(self):
        for device in devices:
            for dtype in (th.complex64, th.complex128):
                th_kwargs = {"dtype": dtype, "device": device}
                a = th.empty((2, 2), **th_kwargs)

                a.clone()

    def test_tensor_factories(self):
        for device in devices:
            for dtype in (th.complex64, th.complex128):
                th_kwargs = {"dtype": dtype, "device": device}
                np_kwargs = {"dtype": dtype_map[dtype]}
                tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).tiny}
                a = th.tensor([[1+1j, 1-1j], [3.-2.51j, 3.+2.51j]], **th_kwargs)

                assert_allclose(t2n(th.zeros((2, 2), **th_kwargs)), np.zeros((2, 2), **np_kwargs))
                assert_allclose(t2n(th.ones((2, 2), **th_kwargs)), np.ones((2, 2), **np_kwargs), **tol_kwargs)
                assert_allclose(t2n(th.full((2, 2), 1+1j, **th_kwargs)), np.full((2, 2), 1+1j, **np_kwargs), **tol_kwargs)
                assert_allclose(t2n(th.scalar_tensor(1+1j, **th_kwargs)), np.complex64(1+1j), **tol_kwargs)
                # assert_allclose(t2n(th.eye(2, **th_kwargs)), np.eye(2, **np_kwargs), **tol_kwargs)

                th.empty_like(a)
                th.zeros_like(a)
                th.ones_like(a)
                th.full_like(a, 1.0+1.0j)

                # th.randint(10, (4, 4), **th_kwargs)  # _th_random_
                # th.randint_like(a)  # _th_random_
                # th.rand((4, 4), **th_kwargs)  # th_uniform
                # th.rand_like(a)  # th_uniform
                # th.randn((4, 4), **th_kwargs)  # th_normal
                # th.randn_like(a)  # th_normal
                # th.randperm(10, **th_kwargs)
                # th.randperm_like(a)

    def test_range_factories(self):
        for device in devices:
            for dtype in (th.complex64, th.complex128):
                th_kwargs = {"dtype": dtype, "device": device}
                np_kwargs = {"dtype": dtype_map[dtype]}
                tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).tiny}
                assert_allclose(t2n(th.linspace(0 + 0j, 2 + 2j, 3, **th_kwargs)), np.linspace(0 + 0j, 2 + 2j, 3, **np_kwargs), **tol_kwargs)
                assert_allclose(t2n(th.logspace(1 + 1j, 1.1 + 1.1j, 5, **th_kwargs)), np.logspace(1 + 1j, 1.1 + 1.1j, 5, **np_kwargs), rtol=1e-6, atol=3e-6)

    def test_scalar_ops(self):
        for device in devices:
            for dtype in (th.complex128,):  # th.complex64 not working
                th_kwargs = {"dtype": dtype, "device": device}
                tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).tiny}
                a = th.tensor([[1+1j, 1-1j], [2.51-2.51j, 2.51+2.51j]], **th_kwargs)

                assert_allclose(t2n(a + (3.51 + 3.51j)), t2n(a) + (3.51 + 3.51j), **tol_kwargs)
                assert_allclose(t2n(a - (3.51 + 3.51j)), t2n(a) - (3.51 + 3.51j), **tol_kwargs)
                assert_allclose(t2n(a * (3.51 + 3.51j)), t2n(a) * (3.51 + 3.51j), **tol_kwargs)
                assert_allclose(t2n(a / (3.51 + 3.51j)), t2n(a) / (3.51 + 3.51j), **tol_kwargs)

                assert_allclose(t2n((3.51 + 3.51j) + a), (3.51 + 3.51j) + t2n(a), **tol_kwargs)
                assert_allclose(t2n((3.51 + 3.51j) - a), (3.51 + 3.51j) - t2n(a), **tol_kwargs)
                assert_allclose(t2n((3.51 + 3.51j) * a), (3.51 + 3.51j) * t2n(a), **tol_kwargs)
                assert_allclose(t2n((3.51 + 3.51j) / a), (3.51 + 3.51j) / t2n(a), **tol_kwargs)

    def test_binary_ops(self):
        for device in devices:
            for dtype in (th.complex128,):  # th.complex64 not working
                th_kwargs = {"dtype": dtype, "device": device}
                tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).tiny}
                a = th.tensor([[1 + 1j, 1 - 1j], [2.51 - 2.51j, 2.51 + 2.51j],
                               [1 + 1j, 1 - 1j], [2.51 - 2.51j, 2.51 + 2.51j],
                               [1 + 1j, 1 - 1j], [2.51 - 2.51j, 2.51 + 2.51j]], **th_kwargs)

                assert_allclose(t2n(a + a), t2n(a) + t2n(a), **tol_kwargs)
                # assert_allclose(t2n(a - a), t2n(a) - t2n(a), **tol_kwargs)
                assert_allclose(t2n(a * a), t2n(a) * t2n(a), **tol_kwargs)
                assert_allclose(t2n(a / a), t2n(a) / t2n(a), **tol_kwargs)

                assert_allclose(t2n(a == a), t2n(a) == t2n(a), **tol_kwargs)
                assert_allclose(t2n(a != a), t2n(a) != t2n(a), **tol_kwargs)

                # assert_allclose(t2n(a**a), t2n(a)**t2n(a), **tol_kwargs)

    def test_eq_ne(self):
        for device in devices:
            a = th.tensor([[1 + 1j, 1 - 1j], [2.51 - 2.51j, 2.51 + 2.51j]], dtype=th.complex128, device=device)
            assert_allclose(t2n(a == a), t2n(a) == t2n(a))
            assert_allclose(t2n(a != a), t2n(a) != t2n(a))
            byteRes = th.empty_like(a, device=device).byte()
            boolRes = th.empty_like(a, device=device).bool()

            a = th.tensor([[1 + 1j, 1 - 1j], [2.51 - 2.51j, 2.51 + 2.51j]], dtype=th.complex64, device=device)
            assert_allclose(t2n(a == a), t2n(a) == t2n(a))
            assert_allclose(t2n(a != a), t2n(a) != t2n(a))

            a = th.tensor([[1, 1], [2.51, 2.51]], dtype=th.float64, device=device)
            assert_allclose(t2n(a == a), t2n(a) == t2n(a))
            assert_allclose(t2n(a != a), t2n(a) != t2n(a))

            a = th.tensor([[1, 1], [2.51, 2.51], [1, 1], [2.51, 2.51]], dtype=th.float32, device=device)
            assert_allclose(t2n(a == a), t2n(a) == t2n(a))
            assert_allclose(t2n(a != a), t2n(a) != t2n(a))

            a = th.tensor([[1, 1], [2, 2]], dtype=th.int64, device=device)
            assert_allclose(t2n(a == a), t2n(a) == t2n(a))
            assert_allclose(t2n(a != a), t2n(a) != t2n(a))

            a = th.tensor([[1, 1], [2, 2], [1, 1], [2, 2]], dtype=th.int32, device=device)
            assert_allclose(t2n(a == a), t2n(a) == t2n(a))
            assert_allclose(t2n(a != a), t2n(a) != t2n(a))

            a = th.tensor([[1, 1], [2, 2], [1, 1], [2, 2], [1, 1], [2, 2], [1, 1], [2, 2]], dtype=th.int16, device=device)
            assert_allclose(t2n(a == a), t2n(a) == t2n(a))
            assert_allclose(t2n(a != a), t2n(a) != t2n(a))

            a = th.tensor([[1, 1], [2, 2], [1, 1], [2, 2], [1, 1], [2, 2], [1, 1], [2, 2]], dtype=th.uint8, device=device)
            assert_allclose(t2n(a == a), t2n(a) == t2n(a))
            assert_allclose(t2n(a != a), t2n(a) != t2n(a))

    def test_unary_ops(self):
        for device in devices:
            for dtype in (th.complex64, th.complex128,):  # th.complex64 not working
                th_kwargs = {"dtype": dtype, "device": device}
                tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).tiny}
                tol_kwargs_2 = {"rtol": 2*th.finfo(c2r_dtype[dtype]).eps, "atol": 2*th.finfo(c2r_dtype[dtype]).tiny}
                tol_kwargs_8 = {"rtol": 8*th.finfo(c2r_dtype[dtype]).eps, "atol": 8*th.finfo(c2r_dtype[dtype]).tiny}
                a = th.tensor([[1+1j, 1-1j], [-2.51-2.51j, -2.51+2.51j],
                               [1+1j, 1-1j], [-3.51-3.51j, -3.51+3.51j],
                               [1+1j, 1-1j], [-2.51-2.51j, -2.51+2.51j]], **th_kwargs)


                th.sigmoid(a)  # no numpy function
                assert_allclose(t2n(th.reciprocal(a)), np.reciprocal(t2n(a)), **tol_kwargs)
                assert_allclose(t2n(th.neg(a)), np.negative(t2n(a)), **tol_kwargs)

                assert_allclose(t2n(th.sinh(a)), np.sinh(t2n(a)), **tol_kwargs)
                assert_allclose(t2n(th.cosh(a)), np.cosh(t2n(a)), **tol_kwargs)
                assert_allclose(t2n(th.tanh(a)), np.tanh(t2n(a)), **tol_kwargs_2)

                assert_allclose(t2n(th.sin(a)), np.sin(t2n(a)), **tol_kwargs)
                assert_allclose(t2n(th.cos(a)), np.cos(t2n(a)), **tol_kwargs)
                assert_allclose(t2n(th.tan(a)), np.tan(t2n(a)), **tol_kwargs_2)

                assert_allclose(t2n(th.asin(a)), np.arcsin(t2n(a)), **tol_kwargs_8)
                assert_allclose(t2n(th.acos(a)), np.arccos(t2n(a)), **tol_kwargs_8)
                assert_allclose(t2n(th.atan(a)), np.arctan(t2n(a)), **tol_kwargs)

                assert_allclose(t2n(th.exp(a)), np.exp(t2n(a)), **tol_kwargs)
                assert_allclose(t2n(th.log(a)), np.log(t2n(a)), **tol_kwargs)
                assert_allclose(t2n(th.log2(a)), np.log2(t2n(a)), **tol_kwargs)
                assert_allclose(t2n(th.log10(a)), np.log10(t2n(a)), **tol_kwargs)
                assert_allclose(t2n(th.reciprocal(a)), 1/t2n(a), **tol_kwargs)
                assert_allclose(t2n(th.sqrt(a)), np.sqrt(t2n(a)), **tol_kwargs)
                assert_allclose(t2n(th.rsqrt(a)), 1/np.sqrt(t2n(a)), **tol_kwargs_2)

                assert_allclose(t2n(th.abs(a)), np.abs(t2n(a)), **tol_kwargs)
                assert_allclose(t2n(th.angle(a)*180/np.pi), np.angle(t2n(a))*180/np.pi)
                assert_allclose(t2n(th.real(a)), np.real(t2n(a)), **tol_kwargs)
                assert_allclose(t2n(th.imag(a)), np.imag(t2n(a)), **tol_kwargs)
                assert_allclose(t2n(th.conj(a)), np.conj(t2n(a)), **tol_kwargs)

                th.ceil(a)  # numpy does not support complex
                th.floor(a)  # numpy does not support complex
                assert_allclose(t2n(th.round(a)), np.round(t2n(a)))
                th.trunc(a)  # numpy does not support complex

                t2n(th.clamp(a, 2 + 2j, -3 - 3j))
                t2n(th.clamp(a, max=-3 - 3j))
                t2n(th.clamp(a, min=2 + 2j))

    def test_pow(self):
        for device in devices:
            for dtype in (th.complex128,):
                th_kwargs = {"dtype": dtype, "device": device}
                a = th.tensor([[1+1j, 2.51-2.51j], [-2.51-2.51j, -2.51+2.51j],
                               [1+1j, 1-1j], [-3.51-3.51j, -3.51+3.51j],
                               [1+1j, 1-1j], [-2.51-2.51j, -2.51+2.51j]], **th_kwargs)

                b = th.tensor([[1+1j, 1-1j], [-2.51-2.51j, -2.51+2.51j],
                               [1+1j, 1-1j], [-3.51-3.51j, -3.51+3.51j],
                               [1+1j, 1-1j], [-2.51-2.51j, -2.51+2.51j]], **th_kwargs)
                assert_allclose(t2n(a**5), t2n(a)**5)
                assert_allclose(t2n(a**b), t2n(a)**t2n(b))

    def test_real_imag_ops(self):
        for device in devices:
            a = th.tensor([[1+1j, 1-1j], [2.51-2.51j, 2.51+2.51j]], dtype=th.complex128, device=device)
            assert_allclose(t2n(th.real(a)), np.real(t2n(a)))
            assert_allclose(t2n(th.imag(a)), np.imag(t2n(a)))

            a = th.tensor([[1+1j, 1-1j], [2.51-2.51j, 2.51+2.51j]], dtype=th.complex64, device=device)
            assert_allclose(t2n(th.real(a)), np.real(t2n(a)))
            assert_allclose(t2n(th.imag(a)), np.imag(t2n(a)))

            a = th.tensor([[1, 1], [2.51, 2.51]], dtype=th.float64, device=device)
            assert_allclose(t2n(th.real(a)), np.real(t2n(a)))
            assert_allclose(t2n(th.imag(a)), np.imag(t2n(a)))

            a = th.tensor([[1, 1], [2.51, 2.51], [1, 1], [2.51, 2.51]], dtype=th.float32, device=device)
            assert_allclose(t2n(th.real(a)), np.real(t2n(a)))
            assert_allclose(t2n(th.imag(a)), np.imag(t2n(a)))

            a = th.tensor([[1, 1], [2, 2]], dtype=th.int64, device=device)
            assert_allclose(t2n(th.real(a)), np.real(t2n(a)))
            assert_allclose(t2n(th.imag(a)), np.imag(t2n(a)))

            a = th.tensor([[1, 1], [2, 2], [1, 1], [2, 2]], dtype=th.int32, device=device)
            assert_allclose(t2n(th.real(a)), np.real(t2n(a)))
            assert_allclose(t2n(th.imag(a)), np.imag(t2n(a)))

            a = th.tensor([[1, 1], [2, 2], [1, 1], [2, 2], [1, 1], [2, 2], [1, 1], [2, 2]], dtype=th.int16, device=device)
            assert_allclose(t2n(th.real(a)), np.real(t2n(a)))
            assert_allclose(t2n(th.imag(a)), np.imag(t2n(a)))

            a = th.tensor([[1, 1], [2, 2], [1, 1], [2, 2], [1, 1], [2, 2], [1, 1], [2, 2]], dtype=th.uint8, device=device)
            assert_allclose(t2n(th.real(a)), np.real(t2n(a)))
            assert_allclose(t2n(th.imag(a)), np.imag(t2n(a)))

    def test_tensor_compare(self):
        for device in devices:
            for dtype in (th.complex64, th.complex128):  # th.complex64 not working
                th_kwargs = {"dtype": dtype, "device": device}
                tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).tiny}
                a = th.tensor([[1+1j, 1-1j], [0.51, 0.51+0.51j]], **th_kwargs)

                assert_allclose(t2n(th.max(a, -2)[0]), np.max(t2n(a), axis=-2), **tol_kwargs)
                assert_allclose(t2n(th.max(a, -2)[1]), np.argmax(t2n(a), axis=-2), **tol_kwargs)
                assert_allclose(t2n(th.min(a, -2)[0]), np.min(t2n(a), axis=-2), **tol_kwargs)
                assert_allclose(t2n(th.min(a, -2)[1]), np.argmin(t2n(a), axis=-2), **tol_kwargs)

                assert_allclose(t2n(th.where((a == a).type(th.uint8), a, a)),
                                np.where((t2n(a) == t2n(a)), t2n(a), t2n(a)),
                                **tol_kwargs)

                assert_allclose(t2n(th.isnan(a)), np.isnan(t2n(a)))
                # assert_allclose(t2n(th.isclose(a, a)), np.isclose(t2n(a),
                #                 t2n(a)),
                #                 **tol_kwargs) # le_cpu not implemented for ComplexDouble

                # th.allclose(a, a, **tol_kwargs) # le_cpu not implemented for ComplexDouble

    def test_reduce_ops(self):
        for device in devices:
            for dtype in (th.complex128,):  # th.complex64 not working
                th_kwargs = {"dtype": dtype, "device": device}
                tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).tiny}
                a = th.tensor([1 + 1j, 1 - 1j, 2.51 - 2.51j, 2.51 + 2.51j], **th_kwargs)
                b = th.tensor([[1 + 1j, 1 - 1j], [2.51 - 2.51j, 2.51 + 2.51j]], **th_kwargs)

                assert_allclose(t2n(th.sum(a)), np.sum(t2n(a)), **tol_kwargs)
                assert_allclose(t2n(th.prod(a)), np.prod(t2n(a)), **tol_kwargs)

                # Vector Norm
                assert_allclose(t2n(th.norm(a, p=float('inf'))), np.linalg.norm(t2n(a), np.inf), **tol_kwargs)
                assert_allclose(t2n(th.norm(a, p=float('-inf'))), np.linalg.norm(t2n(a), -np.inf), **tol_kwargs)
                assert_allclose(t2n(th.norm(a, p=0)), np.linalg.norm(t2n(a), 0), **tol_kwargs)
                assert_allclose(t2n(th.norm(a, p=1)), np.linalg.norm(t2n(a), 1), **tol_kwargs)
                assert_allclose(t2n(th.norm(a, p=2)), np.linalg.norm(t2n(a), 2), **tol_kwargs)

                # Matrix Norm
                # assert_allclose(t2n(th.norm(b, p=2)), np.linalg.norm(t2n(b), 2), **tol_kwargs) # not equal
                # assert_allclose(t2n(th.norm(b, p=2, dim=(0, 1))), np.linalg.norm(t2n(b), 2), **tol_kwargs)  # _th_zero_  not supported
                assert_allclose(t2n(th.norm(b, p='fro')), np.linalg.norm(t2n(b), 'fro'), **tol_kwargs)
                assert_allclose(t2n(th.norm(b, p='fro', dim=(0, 1))), np.linalg.norm(t2n(b), 'fro'), **tol_kwargs)
                # assert_allclose(t2n(th.norm(b, p='nuc')), np.linalg.norm(t2n(b), 'nuc'), **tol_kwargs)
                # assert_allclose(t2n(th.norm(b, p='nuc', dim=(0, 1))), np.linalg.norm(t2n(b), 'nuc'), **tol_kwargs) # _th_zero_  not supported

                # Vector Norm of Matrix
                assert_allclose(t2n(th.norm(b, p=float('inf'), dim=1)), np.linalg.norm(t2n(b), np.inf, axis=1), **tol_kwargs)
                assert_allclose(t2n(th.norm(b, p=float('-inf'), dim=1)), np.linalg.norm(t2n(b), -np.inf, axis=1), **tol_kwargs)
                assert_allclose(t2n(th.norm(b, p=1, dim=1)), np.linalg.norm(t2n(b), 1, axis=1), **tol_kwargs)
                assert_allclose(t2n(th.norm(b, p=2, dim=1)), np.linalg.norm(t2n(b), 2, axis=1), **tol_kwargs)

                assert_allclose(t2n(th.any(a.type(th.uint8) == a.type(th.uint8))), np.any(t2n(a) == t2n(a)), **tol_kwargs)
                assert_allclose(t2n(th.all(a.type(th.uint8) == a.type(th.uint8))), np.all(t2n(a) == t2n(a)), **tol_kwargs)

                assert_allclose(t2n(th.mean(a)), np.mean(t2n(a)), **tol_kwargs)
                assert_allclose(t2n(th.std(a, 0)), np.std(t2n(a), 0, ddof=1), **tol_kwargs)
                assert_allclose(t2n(th.var(a, 0)), np.var(t2n(a), 0, ddof=1), **tol_kwargs)
                assert_allclose(t2n(th.var_mean(a, 0)[0]), np.var(t2n(a), 0, ddof=1), **tol_kwargs)
                assert_allclose(t2n(th.var_mean(a, 0)[1]), np.mean(t2n(a)), **tol_kwargs)

                # assert_allclose(t2n(th.min(a, a)), np.minimum(t2n(a), t2n(a)), **tol_kwargs)  # _th_min not supported
                # assert_allclose(t2n(th.max(a, a)), np.maximum(t2n(a), t2n(a)), **tol_kwargs)  # _th_max not supported

    def test_index_ops(self):
        for device in devices:
            for dtype in (th.complex64, th.complex128):  # th.complex64 not working
                th_kwargs = {"dtype": dtype, "device": device}
                tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).tiny}
                a = th.tensor([[1+1j, 1-1j], [0.51, 0.51+0.51j]], **th_kwargs)

                # get/select
                assert_allclose(t2n(a[1, 0]), t2n(a)[1, 0], **tol_kwargs)
                assert_allclose(t2n(a[1, 0:1]), t2n(a)[1, 0:1], **tol_kwargs)
                assert_allclose(t2n(a[1, :]), t2n(a)[1, :], **tol_kwargs)
                assert_allclose(t2n(a[1, ...]), t2n(a)[1, ...], **tol_kwargs)
                indices = th.tensor([0, 1])
                mask = a == 0
                # th.take(a, indices)  # _th_take not implemented
                # th.index_select(a, 0, indices)  # _th_index_select not implemented
                # th.masked_select(a, mask)  # _th_masked_select_bool
                th.narrow(a, 1, 0, 1)

                # set
                # a[1, 0] = 1.0 + 2.0j  # _th_set_ not implemented
                # a[1, 0:1] = a[1, 1:2]  # _th_set_ not implemented
                # a[1, :] = a[0, :]  # _th_set_ not implemented
                # a[1, ...] = a[0, ...]  # _th_set_ not implemented
                # th.gather(a, 1, th.tensor([[0, 0], [1, 0]]))  # _th_gather

                # transpose
                th.transpose(a, 1, 0)
                a.t()

                # cat, split
                # assert_allclose(t2n(th.cat((a, a), 0)), np.cat((t2n(a), t2n(a)), 0), **tol_kwargs) # _th_cat not implemented
                # assert_allclose(t2n(th.stack((a, a), 0)), np.stack((t2n(a), t2n(a)), 0), **tol_kwargs)  # _th_cat not implemented
                # th.nonzero(a)  # _th_nonzero not implemented
                assert_allclose(t2n(th.chunk(a, 2, 1)[0]), np.split(t2n(a), 2, 1)[0], **tol_kwargs)
                assert_allclose(t2n(th.split(a, 1, 1)[0]), np.split(t2n(a), 2, 1)[0], **tol_kwargs)
                assert_allclose(t2n(th.unbind(a, 1)[0]), np.split(t2n(a), 2, 1)[0].flatten(), **tol_kwargs)

                # reshape
                # a.reshape((-1,))  # _th_set_ not implemented
                # a.view((-1,))  # _th_set_ not implemented
                a.unsqueeze(-1).squeeze()

    def test_pointwise_ops(self):
        for device in devices:
            for dtype in (th.complex64, th.complex128):  # th.complex64 not working
                th_kwargs = {"dtype": dtype, "device": device}
                a = th.empty(2, 2, **th_kwargs)

                th.addcmul(a, 0.1 + 0.2j, a, a)  # numpy doesn't support addcmul
                th.addcdiv(a, 0.1 + 0.2j, a, a)  # numpy doesn't support addcmul

    def test_lerp_ops(self):
        for device in devices:
            for dtype in (th.complex64, th.complex128):  # th.complex64 not working
                th_kwargs = {"dtype": dtype, "device": device}
                a = th.empty(2, 2, **th_kwargs)
                w = th.full((2, 2), 0.5, **th_kwargs)

                th.lerp(a, a, 0.5)
                th.lerp(a, a, w)

    def test_linpack(self):
        for device in devices:
            for dtype in (th.complex64, th.complex128):  # th.complex64 not working
                th_kwargs = {"dtype": dtype, "device": device}
                tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).tiny}
                tol_kwargs_2 = {"rtol": 2*th.finfo(c2r_dtype[dtype]).eps, "atol": 2*th.finfo(c2r_dtype[dtype]).tiny}

                # qr
                a = th.tensor([[12. + 12.j, -51. - 51.j, 4. + 4.j],
                               [6. + 6.j, 167. + 167j, -68. - 68.j],
                               [-4. - 4.j, 24. + 24.j, -41. - 41.j]], **th_kwargs)
                b = th.tensor([[0], [0], [0]], **th_kwargs)
                th.solve(b, a)
                th.inverse(a)
                assert_allclose(t2n(th.qr(a)[0]), np.linalg.qr(a)[0], **tol_kwargs_2)
                assert_allclose(t2n(th.qr(a)[1]), np.linalg.qr(a)[1], **tol_kwargs)

                # lu
                b = th.tensor([[0], [0], [0]], **th_kwargs)
                a = th.tensor([[12. + 12.j, -51. - 51.j, 4. + 4.j],
                               [6. + 6.j, 167. + 167j, -68. - 68.j],
                               [-4. - 4.j, 24. + 24.j, -41. - 41.j]], **th_kwargs)
                # a = th.stack((a, a), 0)
                # assert_allclose(t2n(th.lu(a)[0]), sp.linalg.lu(a)[0], **tol_kwargs) # at::ScalarType::BFloat16 not implemented for 'Bool'
                # th.lu_solve(a, a, a)

                # cholesky
                a = th.tensor([[1, -2j],
                               [2j, 5]], **th_kwargs)
                b = th.tensor([[0], [0]], **th_kwargs)
                assert_allclose(t2n(th.cholesky(a)), np.linalg.cholesky(a), **tol_kwargs)
                assert_allclose(t2n(th.cholesky(a)), np.linalg.cholesky(a), **tol_kwargs)
                th.cholesky_solve(b, th.cholesky(a))

                # tri u/l
                b = th.tensor([[0], [0], [0]], **th_kwargs)
                a = th.tensor([[12. + 12.j, -51. - 51.j, 4. + 4.j],
                               [6. + 6.j, 167. + 167j, -68. - 68.j],
                               [-4. - 4.j, 24. + 24.j, -41. - 41.j]], **th_kwargs)
                assert_allclose(t2n(th.triu(a)), sp.triu(a), **tol_kwargs)
                assert_allclose(t2n(th.triangular_solve(b, th.triu(a), upper=True)[0]),
                                sp_linalg.solve_triangular(sp.triu(a), b, lower=False),
                                **tol_kwargs)
                assert_allclose(t2n(th.tril(a)), sp.tril(a), **tol_kwargs)
                assert_allclose(t2n(th.triangular_solve(b, th.tril(a), upper=False)[0]),
                                sp_linalg.solve_triangular(sp.tril(a), b, lower=True),
                                **tol_kwargs)

                # svd
                b = th.tensor([[0], [0], [0]], **th_kwargs)
                a = th.tensor([[12. + 12.j, -51. - 51.j, 4. + 4.j],
                               [6. + 6.j, 167. + 167j, -68. - 68.j],
                           [-4. - 4.j, 24. + 24.j, -41. - 41.j]], **th_kwargs)
                th.svd(a)
                # th.pinverse(a) # _th_mm not implemented
                # th.matrix_rank(a) # crashing
                # th.trapz(a, a)  # _th_fmod_ not implemented

                a = th.tensor([[1., 0.], [0., 1.]], **th_kwargs)
                b = th.tensor([[0], [0], [0]], **th_kwargs)

                # th.geqrf(a)  # _th_geqrf not implemented
                # th.orgqr(a, a)  # _th_orgqr not implemented
                # th.ormqr(a, a, a)  # _th_ormqr not implemented

                # least squares
                # th.gels(b, a)  # _th_gels not implemented
                # th.lstsq(b, a)  # _th_gels not implemented

                # determinant
                # th.det(a)  # _th_fmod_ not implemented
                # th.logdet(a)  # _th_fmod_ not implemented
                # th.slogdet(a)  # _th_fmod_ not implemented

    def test_blas(self):
        for device in devices:
            for dtype in (th.complex64, th.complex128):  # th.complex64 not working
                th_kwargs = {"dtype": dtype, "device": device}
                tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).tiny}
                v2 = th.empty((2,), **th_kwargs)
                v3 = th.empty((3,), **th_kwargs)
                m2 = th.empty((2, 2), **th_kwargs)
                m3 = th.tensor([[12. + 12.j, -51. - 51.j, 4. + 4.j],
                            [6. + 6.j, 167. + 167j, -68. - 68.j],
                            [-4. - 4.j, 24. + 24.j, -41. - 41.j]], **th_kwargs)

                assert_allclose(t2n(th.cross(m3, m3)), np.cross(m3, m3), **tol_kwargs)
                # th.ger(v2, v3)  # _th_ger not implemented
                # th.dot(m2, m2) # _th_dot not implemented
                # th.eig(m2)  # _th_eig not implemented
                # th.mv(m2, v2)  # _th_mv not implemented
                # th.matmul(m2, m2)  # _th_mm not implemented
                # th.mm(m2, m2)  # _th_mm not implemented
                # th.matrix_power(m2, 2)

    def test_spectral_ops(self):
        for device in devices:
            for dtype in (th.complex64, th.complex128):  # th.complex64 not working
                th_kwargs = {"dtype": dtype, "device": device}
                tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).tiny}
                a = th.tensor([[1+1j, 1-1j], [0.51, 0.51+0.51j]], **th_kwargs)

                def to_float(tensor):
                    return th.stack((tensor.real().type(th.float64), tensor.imag().type(th.float64)), -1)

                def to_complex(tensor):
                    tensor = tensor.type(th.complex128)
                    return tensor[..., 0] + 1j*tensor[..., 1]

                assert_allclose(t2n(to_complex(th.fft(to_float(a), 1))), np.fft.fft(t2n(a)), **tol_kwargs)
                assert_allclose(t2n(to_complex(th.ifft(to_float(a), 1))), np.fft.ifft(t2n(a)), **tol_kwargs)


if __name__ == '__main__':
    unittest.main()
