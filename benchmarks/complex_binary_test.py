from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals


import operator_benchmark as op_bench
import torch
from torch_cpu_strided_complex import cpp


"""Microbenchmarks for point-wise binary operator."""


# Configs for pointwise binary ops
binary_ops_configs_short = op_bench.config_list(
    attrs=[
        [512, 512],
    ],
    attr_names=['M', 'N'],
    tags=['short']
)

binary_ops_configs_long = op_bench.config_list(
    attrs=[
        [256, 256],
        [1024, 1024],
    ],
    attr_names=['M', 'N'],
    tags=['long']
)


class BinaryOpBenchmark(op_bench.TorchBenchmarkBase):
    def init(self, M, N, op_func):
        device=torch.device('cpu')
        dtype=torch.complex128
        self.input_one = torch.ones((M, N), dtype=dtype, device=device)
        self.input_two = torch.ones((M, N), dtype=dtype, device=device)
        self.op_func = op_func

    def forward(self):
        return self.op_func(self.input_one, self.input_two)


binary_ops_list = op_bench.op_list(
    attr_names=['op_name', 'op_func'],
    attrs=[
        ['add', torch.add],
        ['sub', torch.sub],
        ['mul', torch.mul],
        ['div', torch.div],
        ['eq', torch.eq],
        ['pow', torch.pow],
    ],
)


op_bench.generate_pt_tests_from_op_list(binary_ops_list,
                                        binary_ops_configs_short + binary_ops_configs_long,
                                        BinaryOpBenchmark)


if __name__ == "__main__":
    op_bench.benchmark_runner.main()
