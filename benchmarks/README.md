# Benchmarks

Preliminary Benchmarks are provided [here](https://docs.google.com/spreadsheets/d/17pObcrSTpV4BOOX9FYf1vIX3QUlEgQhLvL1IBEyJyzs/edit?usp=sharing)

The benchmark data compares:

 - double: 64-bit real floating-point ops.
 - 2double: 2X 64-bit = 128-bit real floating-point ops.
 - complex<double>: 128-bit complex floating-point ops.
 
 
## Assumptions

The Pytorch operators benchmark is applied with the following settings:

- Tag: short
- Mode: Eager
- Name: sub_M512_N512
- Input: M: 512, N: 512


